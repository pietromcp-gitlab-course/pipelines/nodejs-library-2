module.exports = function hello(to) {
	return `Hello, ${to || 'World'}!`
}